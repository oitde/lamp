## Synopsis

This code is used to deploy a basic LAMP stack to an Ubuntu host.  It should be run from the host on which you want it deployed, Using an ansible playbook

## Usage

```
./install.sh
```
